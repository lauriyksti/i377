package controller;

import dao.CustomerDao;
import domain.Customer;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Resource
    private CustomerDao customerDao;

    @GetMapping("/customers")
    public List<Customer> getCustomers(){
        return customerDao.getCustomers();
    }

    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable Long id){
        return customerDao.getCustomerById(id);
    }

    @PostMapping("/customers")
    public void insertCustomer(@RequestBody @Valid Customer customer){
        customerDao.insertCustomer(customer);
    }

    @DeleteMapping("/customers")
    public void deleteCustomers(){
            customerDao.deleteAllCustomers();
    }

    @DeleteMapping("/customers/{id}")
    public void deleteCustomerById(@PathVariable Long id){
            customerDao.deleteCustomerById(id);
    }

    @GetMapping("/customers/search")
    public List<Customer> search(@RequestParam(defaultValue = "") String key) {
        return customerDao.searchCustomer(key);
    }
}