package controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController {

    @PreAuthorize("#userName == authentication.name || hasRole('ADMIN')")
    @GetMapping("/users/{userName}")
    public String getUserByName(@PathVariable String userName) {
        System.out.println(userName);
        return "UserByName";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/users")
    public String getAllUsers() {
        System.out.println("Admin asks for all users");
        return "All users";
    }

}