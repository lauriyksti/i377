package dao;

import java.util.List;

import domain.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by lauriu
 * on 26.09.2017.
 */

@Repository
public class CustomerDao {

    @PersistenceContext
    private EntityManager em;

    public List<Customer> getCustomers() {
        return em.createQuery("select distinct c from Customer c left join fetch c.phones", Customer.class)
                .getResultList();
    }

    public Customer getCustomerById(Long id) {
        return em.createQuery(
                "select c from Customer c left join fetch c.phones where c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<Customer> searchCustomer(String key) {
        return em.createQuery(
                " select c from Customer c left join fetch c.phones where upper(c.firstName) like :key " +
                        " or upper(c.lastName) like :key or upper(c.code) like :key ", Customer.class)
                .setParameter("key", '%' + key.toUpperCase() + '%')
                .getResultList();
    }

    @Transactional
    public void insertCustomer(Customer customer) {
        em.persist(customer);
    }

    @Transactional
    public void deleteCustomerById(Long id) {
        em.remove(em.createQuery("select c from Customer c left join fetch c.phones where c.id = :id", Customer.class)
                .setParameter("id", id).getSingleResult());
    }

    @Transactional
    public void deleteAllCustomers() {
        em.createQuery("delete from Customer").executeUpdate();
    }
}