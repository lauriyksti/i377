package domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by lauriu
 * on 26.09.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Customer {

    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "seq1", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Long id;

    @Column(name = "first_name")
    @NotNull
    @Size(min = 2, max = 15)
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    @Size(min = 2, max = 15)
    private String lastName;

    @NotNull
    @Size(min = 2, max = 15)
    @Pattern(regexp = "^[A-Za-z0-9]*$")
    private String code;

    @Size(min = 2, max = 15)
    private String type;

    @OneToMany(cascade = CascadeType.ALL,
                fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "customer_id")
    private List<Phone> phones;
}
