package domain;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by lauriu
 * on 28.10.2017.
 */

@Data
@Entity
public class Phone {
    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "seq2", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Long id;
    private String type;
    private String value;
}
