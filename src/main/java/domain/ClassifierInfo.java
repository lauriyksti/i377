package domain;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * Created by lauriu
 * on 28.10.2017.
 */

@Data
public class ClassifierInfo {
    private List<String> phoneTypes;
    private List<String> customerTypes;

    public ClassifierInfo(){
        this.setPhoneTypes(Arrays.asList("phone_type.fixed", "phone_type.mobile"));
        this.setCustomerTypes(Arrays.asList("customer_type.private", "customer_type.corporate"));
    }
}
