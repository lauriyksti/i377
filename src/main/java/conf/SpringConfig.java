package conf;

import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@PropertySource(value = "classpath:application.properties")
@ComponentScan(basePackages = {"conf", "controller", "dao", "validation"})
@Import({JpaConfig.class})
public class SpringConfig implements WebMvcConfigurer {}