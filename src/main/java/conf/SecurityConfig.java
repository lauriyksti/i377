package conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import security.ApiAuthenticationFilter;
import security.handlers.*;


import javax.servlet.Filter;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(basePackages = {"conf"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/api/classifiers").permitAll()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/api/users").hasAnyRole("ADMIN")
                .antMatchers("/api/**").hasAnyRole("USER", "ADMIN");
        http.exceptionHandling().authenticationEntryPoint(new ApiEntryPoint());
        //http.formLogin();
        http.exceptionHandling().accessDeniedHandler(new ApiAccessDeniedHandler());
        http.logout().logoutUrl("/api/logout").logoutSuccessHandler(new ApiLogoutSuccessHandler());

        http.addFilterAfter(
                apiLoginFilter("/api/login"),
                LogoutFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {

//        builder.inMemoryAuthentication()
//                .passwordEncoder(new BCryptPasswordEncoder())
//                .withUser("user")
//                .password("$2a$10$P5c2TZunP/LZ9xGSQShu7.ExmN/4H0H2AWG7EhLWZz/FolVqT6nae")
//                .roles("USER");

        builder.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(new BCryptPasswordEncoder());

    }

    public Filter apiLoginFilter(String url) throws Exception {
        ApiAuthenticationFilter filter = new ApiAuthenticationFilter(url);

        filter.setAuthenticationManager(authenticationManager());

        // add success and failure handlers
        filter.setAuthenticationSuccessHandler(new ApiAuthSuccessHandler());
        filter.setAuthenticationFailureHandler(new ApiAuthFailureHandler());


        return filter;
    }
}